Mobile Cache
************

This module is intented to maintain Drupal core caching for multiple device
types simultaneously through interception and modification of the requested URL.
It performs device detection using the Mobile_Detect class, provided by the
required mobile_detect module.

Mobile cache also provides the capability to switch themes for mobile (and,
optionally, tablet) visitors.

The caching method should be compatible with alternate cache backends such as
memcached as well.

Varnish users may wish to detect and handle multiple devices
at the Apache level rather than at the application level, as I am not sure this
method is compatible with a reverse-proxy cache like Varnish due to the point
at which the URL is modified (likely too late in the request process). I have
not tested this, however.

Other modules that perform mobile-specific features, like theme switching, etc.
(although that feature is provided in this module in a simple fashion) should
be able to take advantage of the caching provided by mobile_cache.

** NOTE: Currently, this module only supports Apache web servers. Support for
others is likely possible, but I do not have an environment to test this in.

Reason for writing the module:
===============================
Certain mobile device detection and theme switching modules are not compatible
with Drupal's caching system.

Since the page for a given URL is built and cached
upon first access, the cached copy will reflect being built for whichever device
the first viewer happened to be using. If a desktop user visits a page,
subsequent mobile users will receive the cached desktop version of the page
rather than the device-appropriate version.

This module addresses this issue.

Theory of Operation:
=====================
Drupal caching operates at the URL level, so pages are cached based on their
paths. This module performs device detection very early in the request process,
sets a cookie reflecting the results, and then appends a query string to the
page URL with device information in it. The cache is then forced to maintain
different copies of pages for different devices.

Installation:
==============
Installation requires modification of the site's settings.php file.

The mobile_detect module must also be installed and configured properly in
order for mobile_cache to operate.

Install the mobile_cache module as normal, and then examine the
mobile_cache/settings.inc file. The comments contain an example line of code to
be added to settings.php:

include DRUPAL_ROOT . '/sites/all/modules/contrib/mobile_cache/settings.inc';

Be sure to modify the module path as necessary - a multi-site installation
might be something similar to (be sure this is all on one line):

include DRUPAL_ROOT . '/sites/example.com/modules/contrib/mobile_cache/
settings.inc';

Once this is complete, caching for multiple devices should be operational. It
may be necessary to clear the site caches first.

To test caching, open the network tab of the chrome inspector or Firebug and
examine the response headers - return visits to a page should result in the
'X-Drupal-Cache: HIT' header being returned. The chrome inspector allows easy
switching of user agents, so simulated cache testing across devices can be
observed.

NOTE: During testing, it will be necessary to delete the 'device' cookie between
page loads when changing devices via the user agent switcher, or your device
change will not be detected.

Theme switching:
=================
This module also allows administrators to define an alternate theme for mobile
devices (and optionally to serve this same alternate theme to tablets).

Visit the configuration page at admin/config/system/mobile-cache to enable
this feature and enter the name of the desired mobile theme.

NOTE: The module will check the machine name of the theme against all available
themes, but be sure this theme is also enabled or theme switching will not
work (in other words, it is possible to enter a valid name for a disabled
theme).
