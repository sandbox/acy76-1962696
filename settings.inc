<?php

/**
 * @file
 * This file should be included in your site's settings.php,
 * for example by adding this line at the end of the file:
 *
 * include DRUPAL_ROOT . '/sites/all/modules/contrib/mobile_cache/settings.inc';
 *
 * Modify path as required by your environment.
 */

require_once dirname(__FILE__) . '/mobile_cache.module';
mobile_cache_preboot();
